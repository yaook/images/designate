FROM python:3.12.6-slim-bookworm

ARG openstack_release=2024.1
ARG branch=stable/${openstack_release}

ENV DESIGNATE_UID=2500018
ENV DESIGNATE_GID=2500018

COPY patches/ /patches

RUN set -eux ; \
    export LANG=C.UTF-8 ; \
    #
    BUILD_PACKAGES="git pkg-config build-essential libssl-dev libffi-dev libapr1-dev libaprutil1-dev python3-dev libmariadb-dev pkg-config" ; \
    REQUIRED_PACKAGES="curl libmariadb3" ; \
    #
    apt-get update ; \
    apt-get install -y --no-install-recommends ${BUILD_PACKAGES} ${REQUIRED_PACKAGES}; \
    #
    addgroup --system --gid $DESIGNATE_GID designate ; \
    adduser --system \
            --home /var/lib/designate \
            --uid $DESIGNATE_UID \
            --gid $DESIGNATE_GID \
            --shell /bin/false designate ; \
    #
    (git clone https://opendev.org/openstack/requirements.git --depth 1 --branch ${branch} \
      || git clone https://opendev.org/openstack/requirements.git --depth 1 --branch ${openstack_release}-eol \
      || git clone https://opendev.org/openstack/requirements.git --depth 1 --branch unmaintained/${openstack_release}); \
    (git clone https://opendev.org/openstack/designate.git --depth 1 --branch ${branch} \
      || git clone https://opendev.org/openstack/designate.git --depth 1 --branch ${openstack_release}-eol \
      || git clone https://opendev.org/openstack/designate.git --depth 1 --branch unmaintained/${openstack_release}) ; \
    pip install -U pip ; \
    pip install -c requirements/upper-constraints.txt PyMySQL mysqlclient python-memcached pymemcache designate; \
    # The following patch can only be removed after we upgrade to the next major version of yaook
    patch -d /usr/local/lib/python3.*/site-packages/oslo_messaging -p 2 -i /patches/fix-rabbitmq-exchange-declare-fallback.patch ; \
    #
    mkdir -p /etc/designate; \
    cp /designate/etc/designate/api-paste.ini /etc/designate/; \
    # 
    apt-get purge --auto-remove -y $BUILD_PACKAGES; \
    rm -rf /var/lib/apt/lists/* /patches; \
    rm -rf requirements designate

COPY files/policies_${openstack_release}.yaml /default_policy/policy.yaml

USER designate

CMD ["designate-api"]
